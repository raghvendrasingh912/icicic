package com.selenium.utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;





public class Library {

	public static WebDriver driver;
	public JavascriptExecutor jse=(JavascriptExecutor)driver;
	public String mainwindow;
	public String captcha;
	public ExtentReports logger;
	public ExtentTest test;

	@BeforeTest
	@Parameters({"url","testname"})
	public void launch(String url,String testname) {
		driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get(url);
		logger = new ExtentReports(System.getProperty("user.dir")+"\\ExtentReportResults.html");
		logger.addSystemInfo("Host Name", " ICICI");
		logger.addSystemInfo("OS", "Windows");
		test=logger.startTest(testname);
	}

	public void waitToBeClickabe(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void waitByVisible(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfAllElements(element));
	}
	


	public Properties readProp(String filepath) throws Exception {
		FileInputStream fin = new FileInputStream(filepath);
		Properties prop = new Properties();
		prop.load(fin);
		return prop;

	}
	


	public void WindowHandler(WebDriver driver) {
		mainwindow=driver.getWindowHandle();
		System.out.println("1st"+mainwindow);
		Set<String> set = driver.getWindowHandles();
		System.out.println(set.size());
		Iterator<String> it = set.iterator();
		while (it.hasNext()) {
			System.out.println("2nd"+it.next());
			String child=it.next();
			if(!(child.equals(mainwindow))) {
				driver.switchTo().window(child);
			}
		}

	}

	public void moveToElement(WebElement element) {
		System.out.println("title=" + driver.getTitle());
		Actions action = new Actions(driver);
		waitToBeClickabe(element);
		action.moveToElement(element).build().perform();

	}
	
	public void keyDown() {
		Actions action=new Actions(driver);
		action.keyDown(Keys.ARROW_DOWN).keyUp(Keys.ARROW_DOWN).build().perform();
		action.keyDown(Keys.ENTER).keyUp(Keys.ENTER).build().perform();
	}
	
	public void keydownEnter() throws AWTException {
		Robot robot=new Robot();
		robot.keyPress(java.awt.event.KeyEvent.VK_DOWN);
		robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
		robot.keyRelease(java.awt.event.KeyEvent.VK_DOWN);
		robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
		
	}
	
	public void dropSelectByVisible(WebElement element,String text) {
		Select select=new Select(element);
		select.selectByVisibleText(text);
	}
	
	public void scrollingPage() {
		
		jse.executeScript("");
		 
	}
	
	public void dropSelectByIndex(WebElement element,int index) {
		Select select=new Select(element);
		select.selectByIndex(index);
	}
	
	public void scrollDown() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
				
	}
	public void scrollTillViewElement(WebElement element) {
		jse.executeScript("arguments[0].scrollIntoView(true);",element);
	}
	
	@AfterTest
	public void afterTestMethod() {
		//driver.close();
		 // writing everything to document
		 //flush() - to write or update test information to your report. 
		                logger.flush();
		                //Call close() at the very end of your session to clear all resources. 
		                //If any of your test ended abruptly causing any side-affects (not all logs sent to ExtentReports, information missing), this method will ensure that the test is still appended to the report with a warning message.
		                //You should call close() only once, at the very end (in @AfterSuite for example) as it closes the underlying stream. 
		                //Once this method is called, calling any Extent method will throw an error.
		                //close() - To close all the operation
		                logger.close();
		                
		    
	}
	
	public String setCaptch() {
		return captcha=JOptionPane.showInputDialog("Enter captch value");
	}
	
	public void clickEnter() throws AWTException {
		Robot robot=new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
	}
	
	public void clickWithJavaScriptExecutor(WebElement element) {
		scrollTillViewElement(element);
		jse.executeScript("arguments[0].click();", element);
	}
	
	public  String takingScreenShot() throws WebDriverException, IOException {
		TakesScreenshot tss=(TakesScreenshot)driver;
		String path="C:\\Users\\m1048084\\eclipse-workspace\\Screens\\myimage.png";
		File dest=new File(path);
		FileUtils.copyFile(tss.getScreenshotAs(OutputType.FILE), dest);
		return path;
		
	}
	@AfterMethod
	public void afterMethodCheck(ITestResult result) throws Exception, IOException {
		if(result.getStatus()==ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, test.addScreenCapture(takingScreenShot()));
		}
		
		
	}

}

