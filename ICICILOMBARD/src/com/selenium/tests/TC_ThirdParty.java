package com.selenium.tests;

import java.util.Properties;

import org.testng.annotations.Test;

import com.selenium.pages.AfterSearchPage;
import com.selenium.pages.BuyNewInsurance;
import com.selenium.pages.Buy_Policy;
import com.selenium.pages.Google;
import com.selenium.pages.GuestLoginPage;
import com.selenium.pages.Icicilambardhomepage;
import com.selenium.pages.Motor_Insurance_Third_Party;
import com.selenium.pages.PlansPage;
import com.selenium.pages.RegisterPage;
import com.selenium.utility.Library;

public class TC_ThirdParty extends Library {
	Library lib=new Library();
	
	@Test()
	public void TC_ThirdParty() throws Exception {
		Google google=new Google(driver);
		AfterSearchPage after=new AfterSearchPage(driver);
		Icicilambardhomepage icici=new Icicilambardhomepage(driver);
		BuyNewInsurance bnew=new BuyNewInsurance(driver);
		PlansPage pp=new PlansPage(driver);
		GuestLoginPage gp=new GuestLoginPage(driver);
		RegisterPage rp=new RegisterPage(driver);
		Motor_Insurance_Third_Party mtp=new Motor_Insurance_Third_Party(driver);
		Buy_Policy bp=new Buy_Policy(driver);
		Properties p=lib.readProp("C:\\Users\\m1048084\\eclipse-workspace\\ICICILOMBARD\\src\\com\\selenium\\utility\\data.properties");
		String city=p.getProperty("city");
		String model=p.getProperty("Model");
		String price=p.getProperty("price");
		String mobile=p.getProperty("mobile");
		String email=p.getProperty("email");
		lib.WindowHandler(driver);
		System.out.println(driver.getTitle());
		google.setSearch("icici lombard");
		lib.keydownEnter();
		//google.clickSeach();
		after.ClickLink();
		icici.movetolink();
		icici.moveToMotor();
		icici.clickBuynow();
		bnew.clickBuyNow();
	 	bnew.setCityValue(city);
		bnew.clickCity();
		Thread.sleep(1000);
		lib.keydownEnter();
		//bnew.clickBangalore();
		//bnew.clickCity();
		bnew.setModel(model);
		Thread.sleep(2000);
		//bnew.clickModel();
		lib.keydownEnter();
		//bnew.clickTvs();
		bnew.clickDate();
		bnew.selectDate();
		bnew.setPrice(price);
		bnew.clickPrice();
		bnew.setMobile(mobile);
		bnew.setEmail(email);
		bnew.clickSubmit();
		Thread.sleep(30000);
		//pp.clickBuynow();	
		//lib.scrollDown();
		pp.clickFiveYearsTP();
		pp.clickBuynow();
		mtp.selectProduct("Two wheeler vehicle");
		mtp.clickContinue();
		bp.clickBuyNewRadio();
		Thread.sleep(5000);
		bp.setRtoCity();
		Thread.sleep(10000);
		lib.keydownEnter();
		Thread.sleep(2000);
		bp.selectDay();
		Thread.sleep(2000);
		bp.selectMonth();
		Thread.sleep(2000);
		bp.selectYear();
		Thread.sleep(2000);
		bp.clickType();
		Thread.sleep(2000);
		bp.setOccupation();
		Thread.sleep(2000);
		bp.setFamilySize();
		Thread.sleep(2000);
		bp.setMStatus();
		Thread.sleep(2000);
		bp.setMonthlyMilage();
		Thread.sleep(2000);
		bp.setManufacturer();
		Thread.sleep(2000);
		bp.setModel();
		Thread.sleep(2000);
		bp.setVD();
		Thread.sleep(2000);
		bp.setMobile();
		Thread.sleep(2000);
		bp.setEmail();
		Thread.sleep(2000);
		bp.clickContipre();
		
	}

}
