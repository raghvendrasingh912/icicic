package com.selenium.tests;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.selenium.pages.AfterSearchPage;
import com.selenium.pages.CustomerSpeak;
import com.selenium.pages.Downloads;
import com.selenium.pages.Google;
import com.selenium.pages.Icicilambardhomepage;
import com.selenium.utility.Library;
import com.selenium.utility.VerificationMethods;

public class TC_CustSpeak extends Library {
	
	Library lib =new Library();
	VerificationMethods vm=new VerificationMethods();
	
	@Test
	public void speakTest() throws AWTException, InterruptedException, WebDriverException, IOException {
		logger.startTest("Validating the Customer speak functiolity");
		Google google=new Google(driver);
		Icicilambardhomepage home=new Icicilambardhomepage(driver);
		AfterSearchPage asp=new AfterSearchPage(driver);
		CustomerSpeak cs=new CustomerSpeak(driver);
		Downloads down=new Downloads();
		google.setSearch("icici lombard");
		test.log(LogStatus.INFO, "Search value set perfectly");
		
		lib.keydownEnter();
		test.log(LogStatus.INFO, "going down andclicking the enter button");
		
		asp.ClickLink();
		test.log(LogStatus.INFO, "clicking on ICICI link");
		Assert.assertTrue(false);
		
		cs.setInsuranceType();
		test.log(LogStatus.INFO, "setting up the insurance type");
		
		home.clickDownload();
		test.log(LogStatus.INFO, "clicking on download");
		
		
		WindowHandler(driver);
		test.log(LogStatus.INFO, "Switching from parent to child window");
		//down.selectMotor();
		
		down.clickInsuranve();
		test.log(LogStatus.INFO, "clicking on insurance type ");
		
		
		down.clickOthers();
		test.log(LogStatus.INFO, "Selecting the insurance type value");
		
		down.clickGo();
		test.log(LogStatus.INFO, "click on go button");
		Thread.sleep(5000);
		
		down.clickAllTask();
		test.log(LogStatus.INFO, "clicking on All task");
		logger.endTest(test);
		
	}

}
