package com.selenium.tests;

import java.util.Properties;

import org.testng.annotations.Test;

import com.selenium.pages.AfterSearchPage;
import com.selenium.pages.BuyNewInsurance;
import com.selenium.pages.Google;
import com.selenium.pages.GuestLoginPage;
import com.selenium.pages.Icicilambardhomepage;
import com.selenium.pages.PlansPage;
import com.selenium.pages.RegisterPage;
import com.selenium.utility.Library;

public class TC_MotoTest extends Library {
	Library lib=new Library();
	
	@Test
	public void googleTest() throws Exception {
		
		Google google=new Google(driver);
		AfterSearchPage after=new AfterSearchPage(driver);
		Icicilambardhomepage icici=new Icicilambardhomepage(driver);
		BuyNewInsurance bnew=new BuyNewInsurance(driver);
		PlansPage pp=new PlansPage(driver);
		GuestLoginPage gp=new GuestLoginPage(driver);
		RegisterPage rp=new RegisterPage(driver);
		Properties p=lib.readProp("C:\\Users\\m1048084\\eclipse-workspace\\ICICILOMBARD\\src\\com\\selenium\\utility\\data.properties");
		String city=p.getProperty("city");
		String model=p.getProperty("Model");
		String price=p.getProperty("price");
		String mobile=p.getProperty("mobile");
		String email=p.getProperty("email");
		lib.WindowHandler(driver);
		System.out.println(driver.getTitle());
		google.setSearch("icici lombard");
		lib.keydownEnter();
		//google.clickSeach();
		after.ClickLink();
		icici.movetolink();
		icici.moveToMotor();
		icici.clickBuynow();
		bnew.clickBuyNow();
		bnew.setCityValue(city);
		bnew.clickCity();
		Thread.sleep(1000);
		lib.keydownEnter();
		//bnew.clickBangalore();
		//bnew.clickCity();
		bnew.setModel(model);
		Thread.sleep(1000);
		//bnew.clickModel();
		lib.keydownEnter();
		//bnew.clickTvs();
		bnew.clickDate();
		bnew.selectDate();
		bnew.setPrice(price);
		bnew.clickPrice();
		bnew.setMobile(mobile);
		bnew.setEmail(email);
		bnew.clickSubmit();
		Thread.sleep(30000);
		//pp.clickBuynow();	
		lib.scrollDown();
		//pp.clickFiveYear();
		pp.clickBuynow();
		Thread.sleep(10000);
		gp.clickGuestLogin();
		Thread.sleep(10000);
		rp.setName("A J");
		rp.clickDate();
		rp.selectYear("1995");
		Thread.sleep(2000);
		rp.selectMonth("May");
		Thread.sleep(2000);
		rp.setDate();
		rp.setAdd1("dsk  hdskd  hdsl");
		rp.setPincode("209862");
		rp.setCity("UNNAO");
		rp.clickCity();
		lib.keydownEnter();
		rp.setnominee("Brock Lesner");
		rp.clickRelation();
		Thread.sleep(5000);
		rp.setRelationValue();
		rp.clickNomineeDate();
		rp.selectMonth("May");
		rp.selectYear("2006");
		rp.setDate();
		rp.clickNext();
		rp.setVehicleNumber();
		rp.setEngine();
		rp.setChassis();
		Thread.sleep(2000);
		rp.clickCross();
		//rp.clickLoan();
		//rp.clickLoanValue();
		Thread.sleep(2000);
		rp.clickSecondNext();
		Thread.sleep(5000);
		rp.clickSecondCross();
		rp.clickTC();
		rp.clickProceedToPay();
		
	}

}
