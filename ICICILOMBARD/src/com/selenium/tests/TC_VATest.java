package com.selenium.tests;

import java.awt.AWTException;

import org.testng.annotations.Test;

import com.selenium.pages.AfterSearchPage;
import com.selenium.pages.FeedbackForm;
import com.selenium.pages.Google;
import com.selenium.pages.Icicilambardhomepage;
import com.selenium.utility.Library;

public class TC_VATest extends Library{

	
	
	@Test
	public void vaCheck() throws AWTException, InterruptedException {
		Library lib=new Library();
		Icicilambardhomepage home=new Icicilambardhomepage(driver);
		Google google=new Google(driver);
		AfterSearchPage asp=new AfterSearchPage(driver);
		FeedbackForm ff=new FeedbackForm(driver);
		google.setSearch("icici lombard");
		lib.keydownEnter();
		asp.ClickLink();
		//google.clickSeach
		Thread.sleep(5000);
		//home.clickVA();
		home.clickFeedback();
		ff.setName();
		ff.setEmail();
		ff.setMobile();
		ff.clickProduct();
		Thread.sleep(5000);
		ff.clickCarIns();
		ff.setFeedback();
		ff.enterCaptcha();
		lib.clickEnter();
	}
}
