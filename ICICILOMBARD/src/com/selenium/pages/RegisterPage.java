package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.gargoylesoftware.htmlunit.javascript.host.canvas.WebGLBuffer;
import com.selenium.utility.Library;

public class RegisterPage extends Library{
	
	public RegisterPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_fullname']")
	private WebElement nameval;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_dob']")
	private WebElement dateInput;
	
	@FindBy(how=How.XPATH,using="//select[@class='ui-datepicker-month']")
	private WebElement dateMoth;
	
	@FindBy(how=How.XPATH,using="//select[@class='ui-datepicker-year']")
	private WebElement dateyear;
	
	@FindBy(how=How.XPATH,using="//div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/thead/following::tr/td/a[text()='19']")
	private WebElement date;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_address1']")
	private WebElement add1;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_pincode']")
	private WebElement pincodeVal;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_citystate']")
	private WebElement cityregis;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_nameofnominee']")
	private WebElement nominee;
	
	@FindBy(how=How.XPATH,using="//a[text()='Select']")
	private WebElement relation;
	
	@FindBy(how=How.XPATH,using="//div[contains(@id,'sbHolder_')]/ul/li/a[text()='MOTHER']")
	private WebElement relationValue;
	
	@FindBy(how=How.XPATH,using="//select[@class='ui-datepicker-month']")
	private WebElement dateMothNom;
	
	@FindBy(how=How.XPATH,using="//select[@class='ui-datepicker-year']")
	private WebElement dateyearNom;
	
	@FindBy(how=How.XPATH,using="/a[text()='19']")
	private WebElement dateNom;
	
	@FindBy(how=How.XPATH,using="//a[text()='Next']")
	private WebElement next;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_nominee_dob']")
	private WebElement nomeenidate;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_vgn1']")
	private WebElement vehiclenumber;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_vgn2']")
	private WebElement enginenumber;
	
	@FindBy(how=How.XPATH,using="//input[@id='tw_vgn3']")
	private WebElement chassisnumber;
	
	@FindBy(how=How.XPATH,using="//a[contains(@id,'sbSelector')][text()='None']")
	private WebElement loan;
	
	@FindBy(how=How.XPATH,using="//ul[contains(@id,'sbOptions')]/li/a[text()='None']")
	private WebElement loanValue;
	
	
	@FindBy(how=How.XPATH,using="//a[text()='�']")
	private WebElement cross;
	
	@FindBy(how=How.XPATH,using="//div[@class='innerSectn brdrRdB']/div/following::div/a[text()='Next']")
	private WebElement nextSecond;
	
	@FindBy(how=How.XPATH,using="//div[@class='btnSectn text-center fixed_footer']/a[text()='Proceed To Payment']")
	private WebElement proceedToPay;
	
	@FindBy(how=How.XPATH,using="//input[@id='termsAndC']/following::label")
	private WebElement tandc;
	
	@FindBy(how=How.XPATH,using="//div[@id='Errormsg']/div/a")
	private WebElement secondCross;
	
	
	public void setName(String name) {
		nameval.sendKeys(name);
		}
	
	public void clickDate() {
		dateInput.click();
	}
	
	public void selectMonth(String month) {
		dropSelectByVisible(dateMoth, month);
	}
	
	
	public void selectYear(String year) {
		dropSelectByVisible(dateyear, year);
	}

	public void setDate() {
		date.click();
	}
	
	public void setAdd1(String add) {
		add1.sendKeys(add);
	}

	public void setCity(String city) {
		cityregis.sendKeys(city);
	}
	
	public void clickCity() {
		scrollTillViewElement(cityregis);
		cityregis.click();
	}

	public void setnominee(String name) {
		nominee.sendKeys(name);
	}
	
	public void setPincode(String pincode) {
		pincodeVal.sendKeys(pincode);
	}
	
	public void clickRelation() {
		relation.click();
	}
	
	public void setRelationValue() {
		waitToBeClickabe(relationValue);
		relationValue.click();
	}
	
	public void setYearNominee(String year) {
		dropSelectByVisible(dateyearNom, year);
	}
	
	public void setMonNominee(String month) {
		dropSelectByVisible(dateMothNom, month);
	}
	
	public void setDateNominee() {
		dateNom.click();
	}
	
	public void clickNext() {
		waitByVisible(next);
		next.click();
	}
	
	public void clickNomineeDate() {
		nomeenidate.click();
	}
	
	public void setVehicleNumber() {
		vehiclenumber.sendKeys("UP35N1234");
	}
	
	public void setEngine() {
		enginenumber.sendKeys("ABNHB12345678");
	}
	public void setChassis() {
		chassisnumber.sendKeys("ABCD2345652");
	}
	
	public void clickLoan() {
		loan.click();
	}
	
	public void clickLoanValue() {
		loanValue.click();
	}
	
	public void clickCross() {
		cross.click();
	}
	
	public void clickSecondNext() {
		nextSecond.click();
	}
	
	public void clickProceedToPay() {
		proceedToPay.click();
	}
	
	public void clickTC() {
		scrollTillViewElement(tandc);
		tandc.click();
	}
	
	public void clickSecondCross() {
		secondCross.click();
	}
}