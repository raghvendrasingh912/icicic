package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;
import com.selenium.utility.VerificationMethods;

public class UnderMaintenance extends Library {
	Library lib=new Library();
	VerificationMethods vm=new VerificationMethods();
	public UnderMaintenance(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH,using="//*[text()='ICICI Lombard is temporarily down for maintenance.']")
	private WebElement underMain;
	
	public void verifyUnderMaintenance() {
		vm.isAvialableCheck(underMain);
		
	}
}
