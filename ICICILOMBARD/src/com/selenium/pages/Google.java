package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class Google extends Library{
	public static String serchbox = "//input[@id='lst-ib']";
	public static String clickSearch= "//input[@value='Google Search']";
	
	@FindBy(how=How.XPATH, using="//input[@id='lst-ib']")
	private WebElement searchBoxElement;
	
	@FindBy(how=How.XPATH, using="//input[@value='Google Search']")
	private WebElement clickSearchElement;
	
	public void setSearch(String value) {
		searchBoxElement.sendKeys(value);
	}
	
	public void clickSeach() {
		clickSearchElement.click();
	}
	
	public Google(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
