package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class FeedbackForm extends Library{

	public FeedbackForm(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="txtname")
	private WebElement name;
	
	@FindBy(how=How.ID,using="txtemail")
	private WebElement email;
	
	@FindBy(how=How.ID,using="txtmobile")
	private WebElement mobileNo;
	
	@FindBy(how=How.XPATH,using="//div/a[text()='Select Product']")
	private WebElement product;
	
	@FindBy(how=How.XPATH,using="(//li/a[text()='Car Insurance'])[2]")
	private WebElement car;
	
	@FindBy(how=How.ID,using="txtcomment")
	private WebElement textarea;
	
	@FindBy(how=How.ID,using="txt_captcha_feedbackCharms")
	private WebElement textcap;
	
	public void setName() {
		name.sendKeys("dscsf");
	}
	
	public void setEmail() {
		email.sendKeys("asas@ae.fd");
	}
	
	public void setMobile() {
		mobileNo.sendKeys("9876543210");
	}
	
	public void clickProduct() {
		product.click();
	}
	
	public void clickCarIns() {
		car.click();
	}
	
	public void setFeedback() {
		textarea.sendKeys("this is good");
	}

	public void enterCaptcha() {
		textcap.sendKeys(setCaptch());
	}
}