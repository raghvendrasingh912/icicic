package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class Buy_Policy extends Library{
	
	public Buy_Policy(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//input[@id='ctl00_ContentPlaceHolder1_rdbNew']")
	private WebElement buyNewRadio;
	
	@FindBy(how=How.XPATH,using="//input[@id='ctl00_ContentPlaceHolder1_txtRto']")
	private WebElement rtoCity;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_txtFristRegDate_DayDropDownListCtl']")
	private WebElement day;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_txtFristRegDate_MonthDropDownListCtl']")
	private WebElement month;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_txtFristRegDate_YearDropDownListCtl']")
	private WebElement year;
	
	@FindBy(how=How.XPATH,using="//input[@id='ctl00_ContentPlaceHolder1_rdbIndCompany_0']")
	private WebElement typeRadio;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_ddOccupation']")
	private WebElement occupation;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_ddFamilySize']")
	private WebElement familysize;
	
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_ddMaritalStatus']")
	private WebElement mStatus;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_ddNoofFamilyMembersDriving']")
	private WebElement noOfFamilyVehicle;
	
	@FindBy(how=How.XPATH,using="//select[contains(@id,'ddMonthlyMilag')]")
	private WebElement monthliMilage;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_ddlMake']")
	private WebElement manufacturer;
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_ddlModel']")
	private WebElement model;
	
	@FindBy(how=How.XPATH,using="//input[@id='ctl00_ContentPlaceHolder1_txtVehicleDesc']")
	private WebElement vehicleDesc;
	
	@FindBy(how=How.XPATH,using="//input[@id='ctl00_ContentPlaceHolder1_txtMobileNo']")
	private WebElement mobileNo;
	
	@FindBy(how=How.XPATH,using="//input[@id='ctl00_ContentPlaceHolder1_txtEmail']")
	private WebElement emailID;
	
	@FindBy(how=How.XPATH,using="//input[@id='ctl00_ContentPlaceHolder1_btnCalculatePremium']")
	private WebElement continuePre;
	
	@FindBy(how=How.XPATH,using="//*[contains(@id,'btnContinue')]")
	private WebElement continueBtn;
	
	public void clickBuyNewRadio() {
		buyNewRadio.click();
	}
	
	public void setRtoCity() {
		rtoCity.sendKeys("Bang");
	}
	
	public void selectDay() {
		dropSelectByVisible(day, "04");
		
	}
	
	public void selectMonth() {
		dropSelectByVisible(month, "Oct");
		
	}

	public void selectYear() {
		dropSelectByVisible(year, "2018");
	
	}
	
	public void clickType() {
		scrollTillViewElement(typeRadio);
		typeRadio.click();
	}
	
	public void setOccupation(){
		scrollTillViewElement(occupation);
		dropSelectByVisible(occupation, "BUSINESSMAN - OTHERS");
		
	}
	
	public void setFamilySize() {
		scrollTillViewElement(familysize);
		dropSelectByVisible(familysize, "2");
	}
	
	public void setMStatus() {
		scrollTillViewElement(mStatus);
		dropSelectByVisible(mStatus, "UNMARRIED");
	}
	
	public void setNoOfVehicle(){
		scrollTillViewElement(noOfFamilyVehicle);
		dropSelectByVisible(noOfFamilyVehicle, "2");
		
	}

	public void setMonthlyMilage(){
		waitByVisible(monthliMilage);
		scrollTillViewElement(monthliMilage);
		dropSelectByVisible(monthliMilage,"1000");
	}

	public void setManufacturer(){
		scrollTillViewElement(manufacturer);
		dropSelectByVisible(manufacturer, "BAJAJ");
	}

	public void setModel(){
		scrollTillViewElement(model);
		dropSelectByVisible(model,"BOXER CT" );
	}
		
	public void setVD() {
		scrollTillViewElement(vehicleDesc);
		vehicleDesc.sendKeys("sada");
	
	}
	
	public void setMobile() {
		scrollTillViewElement(mobileNo);
		mobileNo.sendKeys("9876543210");
	
	}
	
	public void setEmail() {
		scrollTillViewElement(emailID);
		emailID.sendKeys("asaew@da.com");
	}
	
	public void clickContipre() {
		scrollTillViewElement(continuePre);
		continuePre.click();
	}
	
	public void clickContinue() {
		continueBtn.click();
	}

}
