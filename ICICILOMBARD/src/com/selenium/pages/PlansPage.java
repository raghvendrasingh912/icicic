package com.selenium.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class PlansPage extends Library {
	
	 public PlansPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		 PageFactory.initElements(driver,this);
	}

	@FindBy(how=How.XPATH,using="//*[text()='9468']")
	private WebElement fiveyears;
	
	@FindBy(how=How.XPATH,using="//*[@id='tw_optionD']")
	private WebElement fiveyearsTPonly;
	
	@FindBy(how=How.XPATH,using="//a[text()='Customize Plan']")
	private WebElement cosmtomize;
	
	@FindBy(how=How.XPATH,using="//div[@class='btnSectn fixed_footer']/a[text()='Buy Now']")
	private WebElement buynow;
	
	public void clickFiveYear() {
		fiveyears.click();
	}
	
	public void clickFiveYearsTP() {
		waitToBeClickabe(fiveyearsTPonly);
		fiveyearsTPonly.click();
	}
	
	public void clickCostomize() {
		cosmtomize.click();
	}
	
	public void clickBuynow() throws AWTException, InterruptedException {
/*		Robot robot=new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);*/
		scrollTillViewElement(buynow);
		waitToBeClickabe(buynow);
		buynow.click();
	}
}
