package com.selenium.pages;

import org.openqa.selenium.By;
import com.selenium.utility.Library;

public class Downloads extends Library {


	By insurance=By.xpath("(//select[@id='innerPg_C008_select_travel_insurance']/following::div[@class='sbHolder'])[1]");
	By claims=By.xpath("//div[@id='sbHolder_52704990']"); 
	By goBtn=By.xpath("//input[@value='Go']");
	By selectMotor=By.xpath("//select[@id='innerPg_C008_select_travel_insurance']");
	By others=By.xpath("(//select[@id='innerPg_C008_select_travel_insurance']/following::div[@class='sbHolder'])[1]/ul/li/a[text()='Others']");
	By allTask=By.xpath("//a[@href='https://www.icicilombard.com/docs/default-source/downloads/all-risk.pdf?sfvrsn=4']");
	
	
	public void clickInsuranve() {
		driver.findElement(insurance).click();
	}
	
	public void clickClaims() {
		driver.findElement(claims).click();
	}
	
	public void clickGo() {
		driver.findElement(goBtn).click();
	}
	
	public void selectMotor() {
		dropSelectByVisible(driver.findElement(selectMotor), "Workmen Compensation");
	}
	
	public void clickOthers() {
		scrollTillViewElement(driver.findElement(others));
		driver.findElement(others).click();
	}
	
	public void clickAllTask() {
		scrollTillViewElement(driver.findElement(allTask));
		waitByVisible(driver.findElement(allTask));
		clickWithJavaScriptExecutor(driver.findElement(allTask));
		//driver.findElement(allTask).click();
	}
}
