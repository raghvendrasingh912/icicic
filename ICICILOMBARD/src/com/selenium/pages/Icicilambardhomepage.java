package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class Icicilambardhomepage extends Library{
	
	Library lib=new Library();
	
	 
	@FindBy(how=How.XPATH , using="//a[@id='Product_link']")
	private WebElement productlink;
	
	@FindBy(how=How.XPATH,using="//a[text()='Claims ']")
	private WebElement cliams;
	
	@FindBy(how=How.XPATH,using="//a[text()='Health Claims']")
	private WebElement healthcliams;
	
	@FindBy(how=How.XPATH,using="//span[text()='Motor Insurance']")
	private WebElement MotorinsuLink;
	
	@FindBy(how=How.XPATH,using="//a[@id='travel_buynow1']")
	private WebElement buynowlink;
	
	@FindBy(how=How.XPATH,using="//div[@class='open']")
	private WebElement va;
	
	@FindBy(how=How.XPATH,using="//a/img/following::span[text()='feedback']")
	private WebElement feedback;
	
	@FindBy(how=How.XPATH,using="//a[text()='Downloads']")
	private WebElement downloadLink;
	
	@FindBy(how=How.XPATH,using="//a[@class='umbrella']/img/following::span[text()='health']")
	private WebElement healthLink;
	
	public Icicilambardhomepage(WebDriver driver) {
		System.out.println("inside cons "+driver.getTitle());
		PageFactory.initElements(driver, this);
	}
	
	public void movetolink() {
		lib.moveToElement(productlink);
	}
	
	public void moveTOClaims() {
		lib.moveToElement(cliams);
	}

	public void moveToMotor() {
		lib.moveToElement(MotorinsuLink);
	}
	
	public void clickBuynow() {
		buynowlink.click();
	}
	
	public void clickVA() {
		va.click();
	}
	
	public void clickFeedback() {
		feedback.click();
	}
	
	public void movetoAndclickHealthClaims() {
		Actions action=new Actions(driver);
		action.moveToElement(healthcliams).click(healthcliams).build().perform();

	}
	
	public void clickDownload() {
		clickWithJavaScriptExecutor(downloadLink);
	}
	
	public void clickHealth() {
		healthLink.click();
	}
}
