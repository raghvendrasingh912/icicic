package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class Motor_Insurance_Third_Party extends Library{
	
	public Motor_Insurance_Third_Party(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.XPATH,using="//select[@id='ctl00_ContentPlaceHolder1_ddlProdList']")
	private WebElement selectProduct;
	
	@FindBy(how=How.XPATH,using="//input[@alternatetext='Continue']")
	private WebElement continueBtn;
	
	public void selectProduct(String product) {
		
		dropSelectByVisible(selectProduct, product);
	}
	
	public void clickContinue() {
		continueBtn.click();
	}
	
}
