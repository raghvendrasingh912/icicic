package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class HealthCarePage extends Library {
	
	public HealthCarePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//a[text()='Register Now!']")
	private WebElement registernow;

	public void clickRegisterNow() {
		waitToBeClickabe(registernow);
		registernow.click();
	}
}
