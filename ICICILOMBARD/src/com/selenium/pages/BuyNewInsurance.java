package com.selenium.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.selenium.utility.Library;

public class BuyNewInsurance extends Library{
	
	public BuyNewInsurance(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH,using="//h2[contains(text(),'Buy New')]")
	private WebElement buyNewlink;
	
	@FindBy(how=How.XPATH ,using="//input[@id='txtRto']")
	private WebElement setCity;
	
	@FindBy(how=How.ID ,using="tw_vd-mybike")
	private WebElement modelvalue;
	
	@FindBy(how=How.ID, using="tw-vd-dob")
	private WebElement date;
	
	@FindBy(how=How.XPATH,using="//*[text()='18']")
	private WebElement datevalue;
	
	@FindBy(how=How.ID,using="tw_vd-showroomprice1")
	private WebElement exprice;
	
	@FindBy(how=How.ID,using="2wMobileNo")
	private WebElement mobile;
	
	@FindBy(how=How.ID,using="2wEmail")
	private WebElement email;
	
	@FindBy(how=How.ID,using="btnsubmit")
	private WebElement submit;
	
	@FindBy(how=How.XPATH,using="//ul[@id='ui-id-2']/li/div[text()='BANGALORE-KARNATAKA']")
	private WebElement bangalore;
	
	@FindBy(how=How.ID, using="//div[@id='ui-id-84'][text()='TVS - APACHE (148CC)']")
	private WebElement tvs;
	
	public void clickBuyNow() {
		buyNewlink.click();
	}
	
	
	public void setCityValue(String city) {
		setCity.sendKeys(city);
	}
	
	public void clickCity() {
		setCity.click();
	}
	
	public void clickBangalore() {
		waitToBeClickabe(bangalore);
		bangalore.click();
	}
	
	public void setModel(String model) {
		waitToBeClickabe(modelvalue);
		modelvalue.sendKeys(model);;
	}
	
	public void clickModel() {
		Actions act=new Actions(driver);
		//modelvalue.click();
		act.click(modelvalue).build().perform();
	}
	
	public void clickTvs() {
		waitToBeClickabe(tvs);
		tvs.click();
	}
	
	public void clickDate() {
		date.click();
	}
	
	public void selectDate() {
		datevalue.click();
	}
	
	public void setPrice(String price) throws AWTException {
		exprice.clear();
		exprice.sendKeys(price);
		/*Robot r=new Robot();
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);*/
	}
	
	public void clickPrice() {
		exprice.click();
	}
	
	public void setMobile(String mobileNo) {
		mobile.sendKeys(mobileNo);
	}
	
	public void setEmail(String emailVal) {
		email.sendKeys(emailVal);
	}
	
	public void clickSubmit() {
		submit.click();
	}
}
