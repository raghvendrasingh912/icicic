package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AfterSearchPage {

	@FindBy(how=How.XPATH ,using="//*[text()='ICICI Lombard™ Insurance | Most Preferred Brand in India‎'] ")
	private WebElement iciclink;
	
	public void ClickLink() {
		iciclink.click();
	}
	
	public AfterSearchPage(WebDriver driver) {
		System.out.println("After"+driver.getTitle());
		PageFactory.initElements(driver, this);
	}
	
}
