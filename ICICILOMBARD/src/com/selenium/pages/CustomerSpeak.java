package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utility.Library;

public class CustomerSpeak extends Library {
	
	public CustomerSpeak (WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//select[@id='Body_C015_drpInsurance']")
	private WebElement insuranceType;
	
	public void setInsuranceType() {
		scrollTillViewElement(insuranceType);
		dropSelectByVisible(insuranceType, "Two Wheeler Insurance");
	}
}
