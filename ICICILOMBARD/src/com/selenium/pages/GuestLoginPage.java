package com.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class GuestLoginPage {
	
	public GuestLoginPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how=How.XPATH ,using="//a[text()='Guest Login']")
	private WebElement guest;
	
	public void clickGuestLogin() {
		guest.click();
	}

}
